﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Deathball : MonoBehaviour
{
    private Transform myLocation;
    Vector3 startPos;
  
   
   void Start()
   {
       myLocation = this.transform;
       startPos = myLocation.position;
   }
    void OnCollisionEnter(Collision c)
    {
       if(c.gameObject.tag == "Player")
        {
            Scene scene = SceneManager.GetActiveScene(); 
            SceneManager.LoadScene(scene.name);
        } 
        else if(c.gameObject.tag == "Death")
        {
          
            gameObject.transform.position = startPos;
            gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        }
    }
}
