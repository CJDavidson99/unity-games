﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAimCamera : MonoBehaviour
{
    public GameObject target;
    public float rotateSpeed = 5;
    Vector3 offset;

    void Start()
    {
        offset = target.transform.position - transform.position;
    }


    void LateUpdate()
    {
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        float vertical = -(Input.GetAxis("Mouse Y") * rotateSpeed);
        target.transform.Rotate(vertical, horizontal, 0);
        

        float desiredAngle = target.transform.eulerAngles.y;
        float verticalAngle = target.transform.eulerAngles.x;
        Quaternion rotation = Quaternion.Euler(verticalAngle, desiredAngle, 0);
        transform.position = target.transform.position - (rotation * offset);
   
        transform.LookAt(target.transform);
    }
}
