﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int score = 0;
    public Rigidbody rb;
    public float movespeed = 1f;
    public float topspeed = 10f;
    public float jump = 1f;
    public bool grounded = true;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.W) && grounded == true)
        {
            rb.AddForce(0,jump,0);
            grounded = false;
        }
        if(Input.GetKey(KeyCode.D))
        {
            rb.AddForce(movespeed,0,0);
        }
        if(Input.GetKey(KeyCode.A))
        {
            rb.AddForce(-(movespeed),0,0);
        }
        if(Input.GetKey(KeyCode.S))
        {
            rb.AddForce(0, -50, 0);
        }
        float tmpY = rb.velocity.y;
        rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, topspeed);

        rb.velocity = new Vector3(rb.velocity.x, tmpY, rb.velocity.z);
        if(Input.GetKey(KeyCode.A) == false && Input.GetKey(KeyCode.D) == false)
        {
            rb.AddForce(-(rb.velocity.x / 0.4f), 0, 0);
        }
    }
    void OnCollisionEnter(Collision c)
    {
         if(c.gameObject.tag == "Pickup")
        {
            Destroy(c.gameObject);
            score += 1;
            Debug.Log(score);
            AudioSource a = GetComponent<AudioSource>();
            a.Play();
        }
     //   else if(c.gameObject.tag == "Death")
     //   {
     //       transform.position = new Vector3(0,0,0);
     //   }
        else
        {
        grounded = true;
        //Debug.Log(c.gameObject.tag);
        }
    }
}
