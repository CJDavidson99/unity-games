﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
     private Rigidbody rb;
    public float jump = 50f;
    private bool grounded = true;
    private bool dbljump = true;
    private bool keyrelease = true;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
         if(Input.GetKeyDown(KeyCode.Space) && grounded == true && keyrelease == true)
        {
            rb.AddForce(0,jump,0);
            grounded = false;
            keyrelease = false;
        }
        if(Input.GetKeyUp(KeyCode.Space))
        {
            keyrelease = true;
        }
         if(Input.GetKeyDown(KeyCode.Space) && grounded == false && dbljump == true && keyrelease == true)
        {
            rb.AddForce(0,jump,0);
            dbljump = false;
            keyrelease = false;
        }
    }
    void OnCollisionEnter(Collision c)
    {
        grounded = true;
        dbljump = true;
    }

    public bool getGrounded(){
        return grounded;
    }
}
