﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public float tx = 0f;
    public float ty = 0f;
    public float tz = 0f;
    void OnCollisionEnter(Collision c)
    {
       if(c.gameObject.tag == "Player")
        {
            c.gameObject.transform.position = new Vector3(tx,ty,tz);
        } 
    }
    
//Vector3 temp = new Vector3(7.0f,0,0);
//myGameObject.transform.position = temp;
}
