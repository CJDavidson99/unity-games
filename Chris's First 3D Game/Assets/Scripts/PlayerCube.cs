﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCube : MonoBehaviour
{
public GameObject player;
//private vector3 pos;


    void Update()
    {
        float x = transform.eulerAngles.x;
        float y = transform.eulerAngles.y;
        transform.rotation = Quaternion.Euler(x,y,0);
        transform.position = player.transform.position;
    }
}
