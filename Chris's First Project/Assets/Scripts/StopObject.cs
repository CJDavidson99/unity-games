﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopObject : MonoBehaviour
{

    void OnCollisionEnter(Collision c)
    {
       c.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
    }
}
