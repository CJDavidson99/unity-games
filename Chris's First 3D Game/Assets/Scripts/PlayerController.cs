﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
//public float brake;
    public float topspeed = 10f;
    public GameObject target;
    private Rigidbody rb;
   // private PlayerJump pj;
   // private float airspeed;
    
    // public float jump = 50f;
    // private bool grounded = true;
   
   void Start ()
   {
       rb = GetComponent<Rigidbody>();
    //   pj = GetComponent<PlayerJump>();
   }
   
//    void Update()
//    {
//        if(Input.GetKeyDown(KeyCode.Space) && grounded == true)
//         {
//             rb.AddForce(0,jump,0);
//             grounded = false;
//         }
//    }
    void FixedUpdate ()
    {
        
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        var forward = target.transform.forward;
        var right = target.transform.right;

        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();

        Vector3 moveDirection = forward * moveVertical + right * moveHorizontal;
// if(pj.getGrounded() == true)
// {
//     airspeed = 1f;
// }
// else
// {
//     airspeed = 0.8f;
// }
// Debug.Log(airspeed);
        
        rb.AddForce (moveDirection * speed);
       float tmpY = rb.velocity.y;
       rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
       rb.velocity = Vector3.ClampMagnitude(rb.velocity, topspeed);
      // tmpY = tmpY - rb.velocity.y;
       // rb.AddForce(0, tmpY, 0);
       rb.velocity = new Vector3(rb.velocity.x, tmpY, rb.velocity.z);
       // Debug.Log(tmpY);
         if(moveHorizontal == 0f && moveVertical == 0)
    {
        rb.AddForce(-(rb.velocity.x / 0.1f), 0, -(rb.velocity.z / 0.1f));
    }
       // Debug.Log(rb.velocity.y);
       // Debug.Log(pj.getGrounded());
       // Debug.Log(moveDirection.magnitude);
    //    float tempx = 0;
    //    if(rb.velocity.x > topspeed)
    //    {
    //     tempx = topspeed;
    //     if(tempx < 0)
    //    }
       // Debug.Log(rb.velocity.magnitude);
        //Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

        //rb.AddForce (movement * speed);
    }

    // void OnCollisionEnter(Collision c)
    // {
    //     grounded = true;
    // }

}
