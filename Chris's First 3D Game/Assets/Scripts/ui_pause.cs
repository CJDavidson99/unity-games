﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ui_pause : MonoBehaviour
{
    public GameObject pause;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(pause.activeSelf)
            {
                pause.SetActive(false);
                Time.timeScale = 1;
            }
            else
            {
                pause.SetActive(true); 
                Time.timeScale = 0;
            }
        }
    }
     public void ChangeScene(int sceneIndex)
    {
        Time.timeScale = 1;
          Debug.Log("sceneBuildIndex to load: " + sceneIndex);
        SceneManager.LoadScene(sceneIndex);
    }
}
